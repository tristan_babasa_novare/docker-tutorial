**Docker 101**

This session will show basics of docker.

---

## Pre-requisites

Docker Installation for Ubuntu

> 1. Update the 'apt' package index.
>>	`sudo apt-get update`	
> 2. Install the latest version of docker-ce.
>>	`sudo apt-get install docker-ce`
> 3. Check docker version
>>	`docker version`
> 4. Run the docker hello world.
>>  `sudo docker run hello-world`
	

For other distros: [Install Docker](https://docs.docker.com/install/)

---

## Hello World

> 1. BusyBox Hello World.
>>	`docker run busybox echo hello world`
> 2. Run a ubuntu container and access it's terminal.
>>	`docker run -it ubuntu`

> - __-i__ tells Docker to connect us to the container's stdin.
> - __-t__ tells Docker that we want a pseudo-terminal.

---
## Docker commands

This container just displays the time every second.

> 1. Non background task.	
>>	`docker run jpetazzo/clock`
> 2. A background task.
>>	`docker run -d jpetazzo/clock`


* docker start/stop <containerId>
* docker ps -a -l -q
* docker logs <containerId>
* docker logs -f <containerId>
* docker attach
---
## Docker Images

* docker build
* docker images
* docker pull
* docker tag

---
## Writing a Docker Image

Our Dockerfile must be in a new, empty directory.

> 1. Create a directory to hold our Dockerfile.
>>	`mkdir myimage`
> 2. Create a Dockerfile inside this directory.
>>	`cd myproject`
>>	`vim hello.sh`  
>>>	` #! /bin/bash`  
>>>	` echo 'hello world!'`  
> 3. Create a Dockerfile
>>	`vim Dockerfile`  
>>>	`FROM ubuntu`  
>>>	`COPY hello.sh /`  
>>> `CMD /hello.sh`  
> 4. Build the image  
>>	`docker build -t hello-world .`
> 5. Run the container  
>>	`docker run hello-world`

## Push conainer to a repository

> 1. Create a [Dockerhub](https://hub.docker.com/) account  
> 2. Docker cli login  
>>	`docker login`  
> 3. Tag you docker image  
>>	`docker tag <image>:<version> <docker-registry-url>:<version>`  
> 4. Push to docker repository
>>	`docker push <docker-registry-url>:<version>`

---
## Excercise

Create a shopping list 

> 1. Retrieve the latest API for the __Shopping List__.
>> `docker pull angelobabs/shoppinglist:1.0.0`  
>> `docker run -p 8080:8080 angelobabs/shoppinglist:1.0.0`  
> 2. Study the swagger api.
> 3. Develop a __Shopping list web application__.
> 4. Build the docker image
> 5. Push to [docker hub](https://hub.docker.com/).





