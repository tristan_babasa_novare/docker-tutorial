package hk.novare.bootcamp.shoppinglist.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hk.novare.bootcamp.shoppinglist.model.Item;
import hk.novare.bootcamp.shoppinglist.repository.ItemRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;


@CrossOrigin("*")
@RestController
@RequestMapping("/shoppinglist")
@Api("/shoppinglists")
public class ShoppingListController {

    private static final Logger LOG = LoggerFactory.getLogger(ShoppingListController.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    ItemRepository itemRepository;

    @GetMapping("/all")
    @ApiOperation(value = "Get All Shopping List", notes = "retrieved all")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    public ResponseEntity<List<Item>> getShoppingList() {
        List<Item> items = itemRepository.findAll();
        return ResponseEntity.ok(items);
    }


    @PostMapping("/add")
    @ApiOperation(value = "Add to Shopping List", notes = "Add an item")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    public ResponseEntity<Item> addToCart(@RequestBody Item item) {
        Item saved = null;
        try {
            LOG.info("Adding to shopping List {}", mapper.writeValueAsString(item));
            saved = itemRepository.save(item);

        } catch (JsonProcessingException e) {
            LOG.error(e.getMessage(), e);
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(saved);
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "Delete from Shopping List", notes = "Delete an item")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 404, message = "Not Found")
    })
    public ResponseEntity<String> deleteItem(@PathVariable("id") Long id) {
        LOG.info("Deleting item {}", id);
        Optional<Item> item = itemRepository.findById(id);

        if(item.isPresent()) {
            itemRepository.deleteById(id);
        } else {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok("deleted");
    }

    @PutMapping("/update")
    @ApiOperation(value = "Delete from Shopping List", notes = "Delete an item")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<Item> updateItem(@RequestBody Item item) {
        Item payload = item;
        Item saved = null;
        try {
            LOG.info("Updating item {}", mapper.writeValueAsString(item));
            Optional<Item> itemFind = itemRepository.findById(payload.getId());
            if(itemFind.isPresent()) {
                saved = itemRepository.save(payload);
            } else {
                return ResponseEntity.notFound().build();
            }

        } catch (JsonProcessingException e) {
            LOG.error(e.getMessage(), e);
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(saved);

    }

}
