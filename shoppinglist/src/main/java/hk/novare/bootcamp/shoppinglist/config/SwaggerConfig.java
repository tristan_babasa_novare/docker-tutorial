package hk.novare.bootcamp.shoppinglist.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

import static springfox.documentation.builders.PathSelectors.regex;

@EnableSwagger2
@Configuration
public class SwaggerConfig {

    @Bean
    public Docket shoppingListApi() {
        return new Docket(DocumentationType.SWAGGER_2).useDefaultResponseMessages(false)
                .select()
                .apis(RequestHandlerSelectors.basePackage("hk.novare.bootcamp.shoppinglist"))
                .paths(regex("/.*"))
//                .paths(Predicates.not(PathSelectors.regex("/basic-error-controller.*")))
                .build()
                .apiInfo(new ApiInfo("API Documentation",
                        "BootCamp Shopping List API",
                        "1.0.0",
                        "",
                        null,
                        "",
                        "",
                        Collections.emptyList()));

    }

}
