package hk.novare.bootcamp.shoppinglist.repository;

import hk.novare.bootcamp.shoppinglist.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item, Long> {
}
